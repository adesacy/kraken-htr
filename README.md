# Kraken-HTR

Un guide pas-à-pas pour lancer un modèle de reconnaissance d'écriture manuscrite (HTR) à partir d'une vérité terrain collectée sur E-Scriptorium (INRIA, https://traces6.paris.inria.fr/) et du moteur de reconnaissance de caractère Kraken (http://kraken.re/master/index.html).

## 1. Corpus

Le corpus utilisé est le manuscrit autographe de *L’Histoire de ma vie* de Giacomo Casanova écrit entre 1789 et sa mort survenue en 1798. Il est disponible sur Gallica à l’adresse suivante : https ://gallica.bnf.fr/ark :/12148/.

## 2. Éléments de transcription

### 2.1. Plan de nommage des fichiers
Il est très important de mettre en place un plan de nommage de fichiers, où les images et les fichiers XML ont un nom correspondants.

### 2.2. Éléments typographiques

- Les accents :
    - Graves pour aigus en permanence.
    - Omission des accents en permanence.

- Les mots soulignés :
    - Les garde-t-on dans la transcription : j’ai opté pour oui, sauf pour les très longs passages sur plusieurs lignes.
- Les mots barrés :
    - Les garde-t-on dans la transcription : j’ai opté pour non, car aucun moyen de transcrire un mot barré grâce à un caractère spécifique. Non prise en compte de la ligne, ou création de deux lignes en omettant le mot barré.
- Les formes archaïques du français :
    - Conjugaisons avec désinences en -ois : ex : je pourrois. Non identifiées au début.
- Les fautes d’orthographes :
    - Elles sont très nombreuses, et dans une perspective de collecte de vérité terrain, on ne peut les corriger.

Toutes ces remarques, si elles paraissent anodines, ne le sont pas forcément au niveau de la mise en place d’un HTR à grande échelle ; un modèle d’HTR performant prendra en compte ces fautes d’orthographes, ces archaïsmes de la langue, ces formes de conjugaisons d’époque. Faut-il les corriger en aval ? Quel est l’objectif poursuivi ? Proposer le manuscrit tel qu’il est, tel qu’il a été écrit ou proposer une version réactualisée, récente et sans faute du manuscrit, notamment dans une perspective de data mining ? Faut-il deux versions ?

## 3. Entraînement des modèles de transcription via Kraken
### 3.1. Installation de Kraken
#### 3.1.1. Références utiles

- Site Kraken : http ://kraken.re/ ; http ://kraken.re/training.html
- Dépôt Github : https ://github.com/mittagessen/kraken/
- https ://digitalintellectuals.hypotheses.org/3844
- Gitlab projet Lectaurep : https ://gitlab.inria.fr/almanach/lectaurep/documentation
- Banque open-source de modèles : https://htr-united.github.io/

#### 3.1.2. Installation

Création d’un environnement virtuel & installation de kraken via le terminal :
- python3 -m venv kraken-env
- source kraken-env/bin/activate
- pip install kraken
- N.B. : resolving issue with torch : pip install –quiet –pre torchvision -f https ://download.pytorch.org/whl/nightly/cpu/torch_nightly.html. Réf : Voir ici https ://github.com/pytorch/pytorch/issues/40013.
- Téléchargement des modèles existants : kraken get 10.5281/zenodo.2577813
- Liste des modèles open-source : kraken list


#### 3.1.3. Commandes pour lancement des entraînements : 

**Entraînement d’un modèle de segmentation :**

- Exportation de la vérité terrain annotée dans E-Scriptorium : sélection des images, import en PageXML.
- Création d’un dossier training data avec les .JEPG et les .xml correspondant à la vérité terrain.
- Lancement de la commande : ketos segtrain -f page -t .xml

**Entraînement d’un modèle de transcription :**

- Exportation de la vérité terrain annotée dans E-Scriptorium : sélection des images, import en PageXML.
- Création d’un dossier training data avec les .JEPG et les .xml correspondant à la vérité terrain.
- Lancement de la commande : ketos train -f page -o <nom du model de sortie> .xml

**Lancement des modèles entraînés :**

- **Binarisation** (Optionnel) : kraken -i <image source> <image créée binarisée> binarize. La segmentation peut faire baisser parfois l'accuracy du modèle.

- **Segmentation** : kraken -i <image source> test.json segment -bl -i <model seg- mentation>. Càd : lancement segmentation sur un tif avec sortie des coordonnées dans un fichier test.json, modele entraîné sur des baselines (-bl) et chemin vers le modèle (-i).
- **Transcription** : kraken -i <image> <output .txt file> binarize segment ocr -m <model>

**Chaînage des fonctions :**

Volonté d’appliquer à la fois le modèle de segmentation sur les images puis le modèle de transcription :
- **Pour tous les fichiers (segmentation + transcription)** : for i in .tif; do kraken -i $i ../ocr/${i%.JPEG}.txt segment -bl -i <model de segmentation> ocr -m .<model de transcription> ; done

## 4. Évaluation des résultats

**Références utiles**

- ocrevalUAtion : https ://github.com/impactcentre/ocrevalUAtion.
- Kraken : permet également de générer des rapports d’évaluation : 
    - Tests lancés grâce à la commande : ketos test -e ../Test/model-15-pages/*.xml -m ../model/Transcription-15-pages-15-12-21_best.mlmodel -f page.
- Kami : application INRIA qui a remplacé Kraken benchmark. https://gitlab.inria.fr/dh-projects/kami/kami-lib

